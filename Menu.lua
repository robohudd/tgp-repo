require "DragAndDrop"

Menu = {}
local MenuShutSprite, MenuOpenSprite, OpenCloseButton, SubMenuSprite
local CropIconSprite, AnimalIconSprite, BuildingIconSprite, DecorIconSprite
local SettingsCogSprite, SettingsMenuSprite, MuteButtonSprite, ResetButtonSprite

function Menu:Initialise()
  Menu.open = false
  Menu.CropMenu = {}
  Menu.CropMenu.open = false
  Menu.AnimalMenu = {}
  Menu.AnimalMenu.open = false
  Menu.BuildingMenu = {}
  Menu.BuildingMenu.open = false
  Menu.DecorMenu = {}
  Menu.DecorMenu.open = false
  Menu.SettingsMenu = {}
  Menu.SettingsMenu.open = false
  
  MenuShutSprite = love.graphics.newImage("MenuAssets/MenuClosed.png")
  MenuOpenSprite = love.graphics.newImage("MenuAssets/MenuOpen.png")
  OpenCloseButton = love.graphics.newImage("MenuAssets/OpenCloseButton.png")
  CropIconSprite = love.graphics.newImage("MenuAssets/CropsIcon.png")
  AnimalIconSprite = love.graphics.newImage("MenuAssets/AnimalsIcon.png")
  BuildingIconSprite = love.graphics.newImage("MenuAssets/BuildingsIcon.png")
  DecorIconSprite = love.graphics.newImage("MenuAssets/DecorationsIcon.png")
  SubMenuSprite = love.graphics.newImage("MenuAssets/SubMenu.png")
  
  --settings menu sprites
  SettingsCogSprite = love.graphics.newImage("MenuAssets/Cog.png")
  SettingsMenuSprite = love.graphics.newImage("MenuAssets/MenuPane.png")
  MuteButtonSprite = love.graphics.newImage("MenuAssets/Mute.png")
  ResetButtonSprite = love.graphics.newImage("MenuAssets/Reset.png")
  
  DragAndDrop:Initialise()
  
  prevMouseDown = false
  mute = false
end

function Menu:Open()
  Menu.open = true
end

function Menu:Close()
  Menu.open = false
  Menu.CropMenu.open = false
  Menu.AnimalMenu.open = false
  Menu.BuildingMenu.open = false
  Menu.DecorMenu.open = false
end

function Menu:KeyPress(key, scancode, isrepeat)

  
end

function Menu:Update()

  collisionQuads = 
  {
     16, 406 + 16, 406 + 16 + 48
  }
  
  local mX, mY = love.mouse.getPosition()

  if Menu.CropMenu.open == true then
    if love.mouse.isDown(1) == true and prevMouseDown == false then  
      prevMouseDown = true
      if CheckCollision((32 * 3), 406 + 16, 32, 32, mX, mY) == true then
        DragAndDrop:SelectSprite("carrots")
      elseif CheckCollision((32 * 3), 406 + 16 + 48, 32, 32, mX, mY) == true then
        DragAndDrop:SelectSprite("potatos")
      elseif CheckCollision((32 * 3), 406 + 16 + (48 * 2), 32, 32, mX, mY) == true then
        DragAndDrop:SelectSprite("wheat")
      elseif CheckCollision((32 * 3), 406 + 16 + (48 * 3), 32, 32, mX, mY) == true then
        DragAndDrop:SelectSprite("pineapples")
      end
    else
      prevMouseDown = false
    end
  end
  
  
  if Menu.BuildingMenu.open == true then
    if love.mouse.isDown(1) == true and prevMouseDown == false then  
      prevMouseDown = true
      if CheckCollision((32 * 9), 406 + 16, 32, 32, mX, mY) == true then
        DragAndDrop:SelectSprite("sprinkler")
      elseif CheckCollision((32 * 9), 406 + 16 + 48, 32, 32, mX, mY) == true then
        DragAndDrop:SelectSprite("fertiliser")
      elseif CheckCollision((32 * 9), 406 + 16 + (48 * 2), 32, 32, mX, mY) == true then
        DragAndDrop:SelectSprite("haybales")
      elseif CheckCollision((32 * 9), 406 + 16 + (48 * 3), 32, 32, mX, mY) == true then
        DragAndDrop:SelectSprite("sheepdog")
      end
    else
      prevMouseDown = false
    end
  end
  
  if Menu.AnimalMenu.open == true then
    if love.mouse.isDown(1) == true and prevMouseDown == false then  
      prevMouseDown = true
      if CheckCollision((32 * 6), 406 + 16, 32, 32, mX, mY) == true then
        DragAndDrop:SelectSprite("chickens")
      elseif CheckCollision((32 * 6), 406 + 16 + 48, 32, 32, mX, mY) == true then
        DragAndDrop:SelectSprite("sheep")
      elseif CheckCollision((32 * 6), 406 + 16 + (48 * 2), 32, 32, mX, mY) == true then
        DragAndDrop:SelectSprite("cows")
      elseif CheckCollision((32 * 6), 406 + 16 + (48 * 3), 32, 32, mX, mY) == true then
        DragAndDrop:SelectSprite("turkeys")
      end
    else
      prevMouseDown = false
    end
  end
  
  if Menu.DecorMenu.open == true then
    if love.mouse.isDown(1) == true and prevMouseDown == false then  
      prevMouseDown = true
      if CheckCollision((32 * 12), 406 + 16, 32, 32, mX, mY) == true then
        DragAndDrop:SelectSprite("dirtPath")
      elseif CheckCollision((32 * 12), 406 + 16 + 48, 32, 32, mX, mY) == true then
        DragAndDrop:SelectSprite("stonePath")
      elseif CheckCollision((32 * 12), 406 + 16 + (48 * 2), 32, 32, mX, mY) == true then
        DragAndDrop:SelectSprite("appleTree")
      elseif CheckCollision((32 * 12), 406 + 16 + (48 * 3), 32, 32, mX, mY) == true then
        DragAndDrop:SelectSprite("house")
      end
    else
      prevMouseDown = false
    end
  end

  prevMouseDown = love.mouse.isDown(1)
end

function Menu:MousePress( x, y, button, istouch )
  if CheckCollision(16, 656 + 16, 32, 32, x, y) == true then
      if Menu.open == false then
        Menu:Open()
      else
        Menu:Close()
      end
      
  end
  
  if CheckCollision(32 * 3, 656 + 16, 32, 32, x, y) == true then
      if Menu.open == true then
        Menu:CloseSubMenus()
        Menu.CropMenu.open = true
      end     
  end
  
  if CheckCollision(32 * 6, 656 + 16, 32, 32, x, y) == true then
      if Menu.open == true then
        Menu:CloseSubMenus()
        Menu.AnimalMenu.open = true
      end     
  end
  
  if CheckCollision(32 * 9, 656 + 16, 32, 32, x, y) == true then
      if Menu.open == true then
        Menu:CloseSubMenus()
        Menu.BuildingMenu.open = true
      end     
  end
  
  if CheckCollision(32 * 12, 656 + 16, 32, 32, x, y) == true then
      if Menu.open == true then
        Menu:CloseSubMenus()
        Menu.DecorMenu.open = true
      end     
  end
  
  if CheckCollision(1200, 10, 64, 64, x, y) == true then
    if Menu.SettingsMenu.open == false then
      Menu.SettingsMenu.open = true
    else
      Menu.SettingsMenu.open = false
    end
  end
  
  if Menu.SettingsMenu.open == true and CheckCollision(880, 30, 256, 64, x, y) == true then
    if mute == false then
      love.audio.setVolume(0.0)
      mute = true
    else
      love.audio.setVolume(1.0)
      mute = false
    end
  end
  
  if Menu.SettingsMenu.open == true and CheckCollision(880, 110, 256, 64, x, y) == true then
    Player:Reset()
  end
  
end
  
function Menu:CloseSubMenus()
  Menu.CropMenu.open = false
  Menu.AnimalMenu.open = false
  Menu.BuildingMenu.open = false
  Menu.DecorMenu.open = false
end

function Menu:Draw()
  if Menu.open == true then
     if Menu.CropMenu.open == true then
      love.graphics.draw(SubMenuSprite, (32 * 3) - 16, 406, 0, 1, 1)
      love.graphics.draw(SmallTileSet, SmallQuads[4], (32 * 3), 406 + 16, 0, 0.5, 0.5)
      love.graphics.print(CarrotCost .. " g", (32 * 3), 406 + 48)
      love.graphics.draw(SmallTileSet, SmallQuads[10], (32 * 3), 406 + 16 + 48, 0, 0.5, 0.5)
      love.graphics.print(PotatoCost .. " g", (32 * 3), 406 + (48 * 2))
      love.graphics.draw(SmallTileSet, SmallQuads[13], (32 * 3), 406 + 16 + (48 * 2), 0, 0.5, 0.5)
      love.graphics.print(WheatCost .. " g", (32 * 3), 406 + (48 * 3))
      love.graphics.draw(SmallTileSet, SmallQuads[7], (32 * 3), 406 + 16 + (48 * 3), 0, 0.5, 0.5)
      love.graphics.print(PineappleCost .. " g", (32 * 3), 406 + (48 * 4))
    end
    if Menu.AnimalMenu.open == true then
      love.graphics.draw(SubMenuSprite, (32 * 6) - 16, 406, 0, 1, 1)
      love.graphics.draw(MediumTileSet, MediumQuads[2], (32 * 6), 406 + 16, 0, 0.25, 0.25)
      love.graphics.print(ChickenCost .. " g", (32 * 6), 406 + 48)
      love.graphics.draw(LargeTileSet, LargeQuads[3], (32 * 6), 406 + 16 + 48, 0, 1 / 6, 1 / 6)
      love.graphics.print(SheepCost .. " g", (32 * 6), 406 + (48 * 2))
      love.graphics.draw(LargeTileSet, LargeQuads[2], (32 * 6), 406 + 16 + (48 * 2), 0, 1 / 6, 1 / 6)
      love.graphics.print(CowCost .. " g", (32 * 6), 406 + (48 * 3))
      love.graphics.draw(MediumTileSet, MediumQuads[3], (32 * 6), 406 + 16 + (48 * 3), 0, 0.25, 0.25)
      love.graphics.print(TurkeyCost .. " g", (32 * 6), 406 + (48 * 4))
    end
    if Menu.BuildingMenu.open == true then
      love.graphics.draw(SubMenuSprite, (32 * 9) - 16, 406, 0, 1, 1)
      love.graphics.draw(SmallTileSet, SmallQuads[17], (32 * 9), 406 + 16, 0, 0.5, 0.5)
      love.graphics.print(SprinklerCost .. " g", (32 * 9), 406 + (48 * 1))
      love.graphics.draw(SmallTileSet, SmallQuads[16], (32 * 9), 406 + 16 + 48, 0, 0.5, 0.5)
      love.graphics.print(FertiliserCost .. " g", (32 * 9), 406 + (48 * 2))
      love.graphics.draw(SmallTileSet, SmallQuads[18], (32 * 9), 406 + 16 + (48 * 2), 0, 0.5, 0.5)
      love.graphics.print(HayBaleCost .. " g", (32 * 9), 406 + (48 * 3))
      love.graphics.draw(SmallTileSet, SmallQuads[19], (32 * 9), 406 + 16 + (48 * 3), 0, 0.5, 0.5)
      love.graphics.print(SheepdogCost .. " g", (32 * 9), 406 + (48 * 4))
    end
    if Menu.DecorMenu.open == true then
      love.graphics.draw(SubMenuSprite, (32 * 12) - 16, 406, 0, 1, 1)
      love.graphics.draw(SmallTileSet, SmallQuads[20], (32 * 12), 406 + 16, 0, 0.5, 0.5)
      love.graphics.print(DirtPathCost .. " g", (32 * 12), 406 + (48 * 1))
      love.graphics.draw(SmallTileSet, SmallQuads[21], (32 * 12), 406 + 16 + 48, 0, 0.5, 0.5)
      love.graphics.print(StonePathCost .. " g", (32 * 12), 406 + (48 * 2))
      love.graphics.draw(SmallTileSet, SmallQuads[1], (32 * 12), 406 + 16 + (48 * 2), 0, 0.5, 0.5)
      love.graphics.print(AppleTreeCost .. " g", (32 * 12), 406 + (48 * 3))
      love.graphics.draw(LargeTileSet, LargeQuads[1], (32 * 12), 406 + 16 + (48 * 3), 0, 1 / 6, 1 / 6)
      love.graphics.print(HouseCost .. " g", (32 * 12), 406 + (48 * 4))
    end
    
    love.graphics.draw(MenuOpenSprite, 0, 656, 0, 1, 1)
    love.graphics.draw(OpenCloseButton, 48, 656 + 16 + 32, 3.1415926, 1, 1)
    love.graphics.draw(CropIconSprite, 32 * 3, 656 + 16, 0, 1, 1)
    love.graphics.draw(AnimalIconSprite, 32 * 6, 656 + 16, 0, 1, 1)
    love.graphics.draw(BuildingIconSprite, 32 * 9, 656 + 16, 0, 1, 1)
    love.graphics.draw(DecorIconSprite, 32 * 12, 656 + 16, 0, 1, 1)
    
  else
    love.graphics.draw(MenuShutSprite, 0, 656, 0, 1, 1)
    love.graphics.draw(OpenCloseButton, 16, 656 + 16, 0, 1, 1)
  end
  
  if Menu.SettingsMenu.open == false then
    --draw cog
    love.graphics.draw(SettingsCogSprite, 1200, 10)
  else
    --draw open settings menu
    love.graphics.draw(SettingsMenuSprite, 816, 10)
    love.graphics.draw(MuteButtonSprite, 880, 30)
    love.graphics.draw(ResetButtonSprite, 880, 110)
    love.graphics.draw(SettingsCogSprite, 1200, 10)
  end
  
end

function CheckCollision(x1, y1, w, h, x2, y2)
  if x2 < x1 then
    return false
  end
  
  if x2 > x1 + w then
    return false
  end
  
  if y2 < y1 then
    return false
  end
  
  if y2 > y1 + h then
    return false
  end
  
  return true
  
end