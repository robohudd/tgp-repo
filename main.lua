require "Menu"
require "Player"


function love.load(arg)
  
  Menu:Initialise()
  Player:Initialise()
  SoundSystem:Initialise()
  Map:LoadSmallTileSet("Resources/Map/SmallTiles.png")
  Map:LoadMediumTileSet("Resources/Map/MediumTiles.png")
  Map:LoadLargeTileSet("Resources/Map/LargeTiles.png")
  LoadGame()
  
  
  
  touchX = 0
  touchY = 0
end

function love.update(dt)

  Map:Update(dt)
  Menu:Update()
  DragAndDrop:Update()
  
  if selectedSprite.value == "null" then
    Camera:Update()
  end
  
end

function love.keypressed(key, scancode, isrepeat)

end

function love.mousepressed( x, y, button, istouch ) 
  Menu:MousePress(x,y,button,istouch)  
  Map:MousePress(x,y,button,istouch)
end

function love.touchpressed(id, x, y, dx, dy, pressure )
  prevX, prevY = x,y
  Menu:MousePress(x,y, 1, true)
end

--function love.touchmoved(id, x, y, dx, dy, pressure )
  
  --if selectedSprite.value == "null" then
    --Camera:setPosition(touchX
  --end
  
--end

function LoadGame()
  local file = io.open("Save.txt", "r")
  if (file == nil) then
    Map:LoadMap("Resources/test.txt")
    return
  end
  io.close(file)
  
  Map:LoadMap("Save.txt")
  
  file = io.open("Save.txt", "r")
  for i=1, Map.Height, 1 do
    file:read()
  end
  
  
  Player.gold = tonumber(file:read())
  
  CarrotTimer = file:read()
  PineappleTimer = file:read()
  PotatoTimer = file:read()
  WheatTimer = file:read()
  ChickenTimer = file:read()
  TurkeyTimer = file:read()
  SheepTimer = file:read()
  CowTimer = file:read()
  
  local year = file:read()
  local month = file:read()
  local day = file:read()
  local hour = file:read()
  local minute = file:read()
  local second = file:read()
  
  local saveTime = (year * 31556926) + (month * 2629743) + (day * 86400) + (hour * 3600) + (minute * 60) + second
  local Now = os.date('*t') --get the date/time
  local currentTime = (Now.year * 31556926) + (Now.month * 2629743) + (Now.day * 86400) + (Now.hour * 3600) + (Now.min * 60) + Now.sec
    
  local timeDiff = currentTime - saveTime
  
  CarrotTimer = CarrotTimer + timeDiff
  PineappleTimer = PineappleTimer + timeDiff
  PotatoTimer = PotatoTimer + timeDiff
  WheatTimer = WheatTimer + timeDiff
  ChickenTimer = ChickenTimer + timeDiff
  TurkeyTimer = TurkeyTimer + timeDiff
  SheepTimer = SheepTimer + timeDiff
  CowTimer = CowTimer + timeDiff

end

function love.quit(r)
  file = io.open("Save.txt", "w")
   
  io.input(file)
  

  for i=1, Map.Width, 1 do
    file:write(Map:GetMapLine(i), "\n")
  end
  
  file:write(Player.gold, "\n")
  
  file:write(CarrotTimer, "\n")
  file:write(PineappleTimer, "\n")
  file:write(PotatoTimer, "\n")
  file:write(WheatTimer, "\n")
  file:write(ChickenTimer, "\n")
  file:write(TurkeyTimer, "\n")
  file:write(SheepTimer, "\n")
  file:write(CowTimer, "\n")
  
  local Now = os.date('*t') --get the date/time
  
  file:write(Now.year, "\n")
  file:write(Now.month, "\n")
  file:write(Now.day, "\n")
  file:write(Now.hour, "\n")
  file:write(Now.min, "\n")
  file:write(Now.sec, "\n")
  
  io.flush(file)
  io.close(file)
end

function love.wheelmoved(x, y)
  local wX = Camera.scaleX
  local wY = Camera.scaleY
  
  Camera:setScale(wX - y, wY - y)
end

function love.draw()
  love.graphics.clear(32, 118, 59, 255)
  
  Camera:set()
  
  Map:DrawMap()
  
  Camera:unset()
  
  Player:Draw()
  Menu:Draw()
  DragAndDrop:Draw()
  
  
end


