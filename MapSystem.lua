require "SoundSystem"

Map = {}
SelectedTile = {}
local deleteBtnSprite

function Map:LoadMap(path)
  
  Map.Width = 25
  Map.Height = 25
  
  --time it takes for the resource to progress to it's next stage
  Map.CarrotsTime = 10
  Map.PineappleTime = 20
  Map.PotatoTime = 30
  Map.WheatTime = 40
  Map.ChickenTime = 50
  Map.TurkeyTime = 60
  Map.SheepTime = 70
  Map.CowTime = 80
   
  --upgrade variables
  Map.SprinklerBuff = 1
  Map.FertiliserBuff = 1
  Map.HayBuff = 1
  Map.SheepDogBuff = 1
  
  CarrotTimer = 0
  PineappleTimer = 0
  PotatoTimer = 0
  WheatTimer = 0
  ChickenTimer = 0
  TurkeyTimer = 0
  SheepTimer = 0
  CowTimer = 0
  
  --how much the player earns from earn resource
  CarrotGold = 15
  PineappleGold = 30
  PotatoGold = 45
  WheatGold = 60
  ChickenGold = 75
  TurkeyGold = 90
  SheepGold = 105
  CowGold = 120
  
  CarrotCost = 10
  PineappleCost = 20
  PotatoCost = 30
  WheatCost = 40
  ChickenCost = 50
  TurkeyCost = 60
  SheepCost = 70
  CowCost = 90
  
  SprinklerCost = 80
  FertiliserCost = 80
  HayBaleCost = 125
  SheepdogCost = 125
  DirtPathCost = 30
  StonePathCost = 30
  AppleTreeCost = 50
  HouseCost = 300
  
  deleteBtnSprite = love.graphics.newImage("MenuAssets/DeleteButton.png")
  SelectedTile.X = 0;
  SelectedTile.Y = 0;

  
  if(path == nil) then
    return nil
  end
  
  local file = io.open(path)
  
  grid = {}
  
  local newLine = "ccccccccccccccccccccccccc"
  
  for line in file:lines() do
    table.insert(grid, line)
  end
  
  io.close(file)
end

function Map:GetMapLine(lineNo)
  local str = grid[lineNo]
  return str
end

function Map:LoadSmallTileSet(path)
  
  SmallTileW = 64
  SmallTileH = 64
  
  SmallTileSet = love.graphics.newImage(path)
  
  local smallTileSetW, smallTileSetH = SmallTileSet:getWidth(), SmallTileSet:getHeight()
  
  SmallQuads = {
    love.graphics.newQuad(0, 0, SmallTileW, SmallTileH, smallTileSetW, smallTileSetH),
    love.graphics.newQuad(64, 0, SmallTileW, SmallTileH, smallTileSetW, smallTileSetH),
    love.graphics.newQuad(128, 0, SmallTileW, SmallTileH, smallTileSetW, smallTileSetH),
    love.graphics.newQuad(0, 64, SmallTileW, SmallTileH, smallTileSetW, smallTileSetH),
    love.graphics.newQuad(64, 64, SmallTileW, SmallTileH, smallTileSetW, smallTileSetH),
    love.graphics.newQuad(128, 64, SmallTileW, SmallTileH, smallTileSetW, smallTileSetH),
    love.graphics.newQuad(0, 128, SmallTileW, SmallTileH, smallTileSetW, smallTileSetH),
    love.graphics.newQuad(64, 128, SmallTileW, SmallTileH, smallTileSetW, smallTileSetH),
    love.graphics.newQuad(128, 128, SmallTileW, SmallTileH, smallTileSetW, smallTileSetH),
    love.graphics.newQuad(0, 192, SmallTileW, SmallTileH, smallTileSetW, smallTileSetH),
    love.graphics.newQuad(64, 192, SmallTileW, SmallTileH, smallTileSetW, smallTileSetH),
    love.graphics.newQuad(128, 192, SmallTileW, SmallTileH, smallTileSetW, smallTileSetH),
    love.graphics.newQuad(0, 256, SmallTileW, SmallTileH, smallTileSetW, smallTileSetH),
    love.graphics.newQuad(64, 256, SmallTileW, SmallTileH, smallTileSetW, smallTileSetH),
    love.graphics.newQuad(128, 256, SmallTileW, SmallTileH, smallTileSetW, smallTileSetH),
    love.graphics.newQuad(0, 320, SmallTileW, SmallTileH, smallTileSetW, smallTileSetH),
    love.graphics.newQuad(64, 320, SmallTileW, SmallTileH, smallTileSetW, smallTileSetH),
    love.graphics.newQuad(128, 320, SmallTileW, SmallTileH, smallTileSetW, smallTileSetH),
    love.graphics.newQuad(0, 384, SmallTileW, SmallTileH, smallTileSetW, smallTileSetH),
    love.graphics.newQuad(64, 384, SmallTileW, SmallTileH, smallTileSetW, smallTileSetH),
    love.graphics.newQuad(128, 384, SmallTileW, SmallTileH, smallTileSetW, smallTileSetH),
    love.graphics.newQuad(192, 64, SmallTileW, SmallTileH, smallTileSetW, smallTileSetH),
    love.graphics.newQuad(192, 128, SmallTileW, SmallTileH, smallTileSetW, smallTileSetH),
    love.graphics.newQuad(192, 192, SmallTileW, SmallTileH, smallTileSetW, smallTileSetH),
    love.graphics.newQuad(192, 256, SmallTileW, SmallTileH, smallTileSetW, smallTileSetH)
  }
end

function Map:LoadMediumTileSet(path)
  
  MediumTileW = 128
  MediumTileH = 128
  
  MediumTileSet = love.graphics.newImage(path)
  
  local mediumTileSetW, mediumTileSetH = MediumTileSet:getWidth(), MediumTileSet:getHeight()
  
  MediumQuads = {
    love.graphics.newQuad(0, 0, MediumTileW, MediumTileH, mediumTileSetW, mediumTileSetH),
    love.graphics.newQuad(128, 0, MediumTileW, MediumTileH, mediumTileSetW, mediumTileSetH),
    love.graphics.newQuad(256, 0, MediumTileW, MediumTileH, mediumTileSetW, mediumTileSetH),
    love.graphics.newQuad(384, 0, MediumTileW, MediumTileH, mediumTileSetW, mediumTileSetH),
    love.graphics.newQuad(512, 0, MediumTileW, MediumTileH, mediumTileSetW, mediumTileSetH)
  }
end

function Map:LoadLargeTileSet(path)
  
  LargeTileW = 192
  LargeTileH = 192
  
  LargeTileSet = love.graphics.newImage(path)
  
  local largeTileSetW, largeTileSetH = LargeTileSet:getWidth(), LargeTileSet:getHeight()
  
  LargeQuads = {
    love.graphics.newQuad(0, 0, LargeTileW, LargeTileH, largeTileSetW, largeTileSetH),
    love.graphics.newQuad(192, 0, LargeTileW, LargeTileH, largeTileSetW, largeTileSetH),
    love.graphics.newQuad(384, 0, LargeTileW, LargeTileH, largeTileSetW, largeTileSetH),
    love.graphics.newQuad(576, 0, LargeTileW, LargeTileH, largeTileSetW, largeTileSetH),
    love.graphics.newQuad(768, 0, LargeTileW, LargeTileH, largeTileSetW, largeTileSetH)
  }
end

function Map:GetTileY(offset)
  
  local tileY = (((love.mouse.getY() - offset) * Camera.scaleY) + Camera.y) / 64
  
  TileY = math.floor(tileY +1)
  
  return TileY
  
end

function Map:GetTileX(offset)
  
  local tileX = (((love.mouse.getX() - offset) * Camera.scaleX) + Camera.x) / 64
  
  TileX = math.floor(tileX + 1)
  
  return TileX
  
end

function Map:GetTileValue(offsetX, offsetY)
  
  local tileX = Map:GetTileX(offsetX)
  local tileY = Map:GetTileY(offsetY)
  
  local str = grid[tileY]
  local sstr = str:sub(tileX, tileX)
  
  return sstr
  
end

function Map:DeleteTile(x, y, size)
  
  local str = grid[y]
  local newStr = "" 
  
  local skipLoop = 0;
  
  for j=0, size - 1, 1
  do
    str = grid[y + j]
    for i=1, Map.Width, 1 
    do
        if skipLoop > 0 then
          skipLoop = skipLoop - 1;
          goto delLoopEnd;
        end
        if i == x then
          if size == 2 then
              newStr = newStr .. "c"
              newStr = newStr .. "c"   
              skipLoop = skipLoop + 1
          elseif size == 3 then
            newStr = newStr .. "c"
            newStr = newStr .. "c"
            newStr = newStr .. "c"
            skipLoop = skipLoop + 2;
          else
            newStr = newStr .. "c"
          end
        else
          newStr = newStr .. str:sub(i, i)
        end
        :: delLoopEnd ::
    end
    grid[y + j] = newStr
    newStr = ""
  end
  
end


-- Function still untested
function Map:SetTileValue(x, y, newValue, size, cost)
  
  if Camera.scaleX ~= 1 or Camera.scaleY ~= 1 then
    return
  end
  
  
  if x + (size - 1) > Map.Width or y + (size - 1) > Map.Height or y < 1 or x < 1 then
    return
  end
  
  local str = grid[y]
  local newStr = ""
  
  local continue = true
  local yStr
  
  if size == 2 or size == 3 then
    print(str)
    if str:sub(x,x) ~= "c" then
      continue = false
    end
    if str:sub(x + 1,x + 1) ~= "c" then
      continue = false
    end
    if size == 3 then
      if str:sub(x + 2, x + 2) ~= "c" then
        continue = false
      end
    end
    yStr = grid[y + 1]
    if yStr:sub(x,x) ~= "c" then
      continue = false
    end
    if yStr:sub(x + 1, x + 1) ~= "c" then
      continue = false
    end   
    if size == 3 then
      if yStr:sub(x + 2, x + 2) ~= "c" then
        continue = false
      end
      yStr = grid[y + 2]
      
      if yStr:sub(x, x) ~= "c" then
        continue = false
      end
      if yStr:sub(x + 1, x + 1) ~= "c" then
        continue = false
      end
      if yStr:sub(x + 2, x + 2) ~= "c" then
        continue = false
      end
    end
  end
  
  if continue == false then
    return
  end
  
  if Player.gold >= cost then
    Player.gold = Player.gold - cost
  else
    return
  end
  
  local skipLoop = 0;
  
  if str:sub(x, x) == "c" then
    for j=0, size - 1, 1
    do
      str = grid[y + j]
      for i=1, Map.Width, 1 
      do
        if skipLoop > 0 then
          skipLoop = skipLoop - 1;
          goto loopEnd;
        end
        if i == x then
          if size == 2 then
            if j == 0 then
              newStr = newStr .. newValue
            else
              newStr = newStr .. "0"
            end
            newStr = newStr .. "0"
            skipLoop = skipLoop + 1;
          elseif size == 3 then
            if j == 0 then
              newStr = newStr .. newValue
            else
              newStr = newStr .. "0"
            end
            newStr = newStr .. "0"
            newStr = newStr .. "0"
            skipLoop = skipLoop + 2;
          else
            newStr = newStr .. newValue
          end
        else
          newStr = newStr .. str:sub(i, i)
        end
        :: loopEnd ::
    end
    grid[y + j] = newStr
    newStr = ""
  end
  end
  
end

function Map:CollectGold()
  
  if love.mouse.isDown(1) then
    local X = Map:GetTileX(0)
    local Y = Map:GetTileY(0)
    local X2 = Map:GetTileX(58)
    local Y2 = Map:GetTileY(58)
    
    for i =1, Map.Height do
      
      local str = grid[i]
      local newStr = ""
        
      for j = 1, Map.Width do
          
        local sstr = str:sub(j, j)
        
        if i == Y and j == X then
          if sstr == "B" then
            newStr = newStr .. "d"
            CarrotTimer = 0
            Player:IncreaseGold(CarrotGold)
            SoundSystem:PlaySound("coin")
          elseif sstr == "C" then
            newStr = newStr .. "g"
            PineappleTimer = 0
            Player:IncreaseGold(PineappleGold)
            SoundSystem:PlaySound("coin")
          elseif sstr == "D" then
            newStr = newStr .. "j"
            PotatoTimer = 0
            Player:IncreaseGold(PotatoGold)
            SoundSystem:PlaySound("coin")
          elseif sstr == "E" then
            newStr = newStr .. "m"
            WheatTimer = 0
            Player:IncreaseGold(WheatGold)
            SoundSystem:PlaySound("coin")
          else
            newStr = newStr .. sstr
          end
        elseif i == Y2 and j == X2 then
          if sstr == "F" then
            newStr = newStr .. "w"
            ChickenTimer = 0
            Player:IncreaseGold(ChickenGold)
            SoundSystem:PlaySound("chicken")
          elseif sstr == "G" then
            newStr = newStr .. "x"
            TurkeyTimer = 0
            Player:IncreaseGold(TurkeyGold)
            SoundSystem:PlaySound("turkey")
          elseif sstr == "H" then
            newStr = newStr .. "A"
            SheepTimer = 0
            Player:IncreaseGold(SheepGold * Map.SheepDogBuff)
            SoundSystem:PlaySound("sheep")
          elseif sstr == "I" then
            newStr = newStr .. "z"
            CowTimer = 0
            Player:IncreaseGold(CowGold * Map.HayBuff)
            SoundSystem:PlaySound("cow")
          else
            newStr = newStr .. sstr
          end
        else
          newStr = newStr .. sstr
        end
      end
    
    grid[i] = newStr
    end
  end
end

function Map:GetTileType(x,y)
  local str = grid[y]
  
  return str:sub(x,x)
end

function Map:MousePress( x, y, button, istouch )
  local pX, pY = Map:GetTileX(0), Map:GetTileY(0)
  
  if button == 1 then
    if CheckMouseCollision( (SelectedTile.X * 64) - 48 - Camera.x, (SelectedTile.Y * 64) - Camera.y, 32, 32, x, y) == true then
      print("COLLISION")
      local tileType = Map:GetTileType(SelectedTile.X, SelectedTile.Y)
      
      if tileType == "0" then
        return
      elseif tileType == "A" or tileType == "z" or tileType == "y" then
        Map:DeleteTile(SelectedTile.X, SelectedTile.Y, 3)
      elseif tileType == "w" or tileType == "x" then
        Map:DeleteTile(SelectedTile.X, SelectedTile.Y, 2)
      else
        Map:DeleteTile(SelectedTile.X, SelectedTile.Y, 1)
      end
          
    end
  end
  

  if button == 2 then
    if pX > 0 and pX < Map.Width and pY > 0 and pY < Map.Height then
      SelectedTile.X = pX
      SelectedTile.Y = pY
    end
  end

end


function Map:Update(dt)
  
  CarrotTimer = CarrotTimer + dt
  PineappleTimer = PineappleTimer + dt
  PotatoTimer = PotatoTimer + dt
  WheatTimer = WheatTimer + dt
  ChickenTimer = ChickenTimer + dt
  TurkeyTimer = TurkeyTimer + dt
  SheepTimer = SheepTimer + dt
  CowTimer = CowTimer + dt

  if CarrotTimer >= Map.CarrotsTime then
  
    for i = 1, Map.Height do
      local str = grid[i]
      local newStr = ""
    
      for j = 1, Map.Width do
        local sstr = str:sub(j,j)
      
        if sstr == "f" then
          newStr = newStr .. "B"
        elseif sstr == "e" then
          newStr = newStr .. "f"
        elseif sstr == "d" then
          newStr = newStr .. "e"
        else
          newStr = newStr .. sstr
        end
      end
    
      grid[i] = newStr
    end
  
    CarrotTimer = CarrotTimer - Map.CarrotsTime
  end

  if PineappleTimer >= Map.PineappleTime then
  
    for i = 1, Map.Height do
      local str = grid[i]
      local newStr = ""
    
      for j = 1, Map.Width do
        local sstr = str:sub(j,j)
      
        if sstr == "i" then
          newStr = newStr .. "C"
        elseif sstr == "h" then
          newStr = newStr .. "i"
        elseif sstr == "g" then
          newStr = newStr .. "h"
        else
          newStr = newStr .. sstr
        end
      end
    
      grid[i] = newStr
    end
  
    PineappleTimer = PineappleTimer - Map.PineappleTime
  end

  if PotatoTimer >= Map.PotatoTime then
  
    for i = 1, Map.Height do
      local str = grid[i]
      local newStr = ""
    
      for j = 1, Map.Width do
        local sstr = str:sub(j,j)
      
        if sstr == "l" then
          newStr = newStr .. "D"
        elseif sstr == "k" then
          newStr = newStr .. "l"
        elseif sstr == "j" then
          newStr = newStr .. "k"
        else
          newStr = newStr .. sstr
        end
      end
    
      grid[i] = newStr
    end
  
    PotatoTimer = PotatoTimer - Map.PotatoTime
  end

  if WheatTimer >= Map.WheatTime then
  
    for i = 1, Map.Height do
      local str = grid[i]
      local newStr = ""
    
      for j = 1, Map.Width do
        local sstr = str:sub(j,j)
      
        if sstr == "o" then
          newStr = newStr .. "E"
        elseif sstr == "n" then
          newStr = newStr .. "o"
        elseif sstr == "m" then
          newStr = newStr .. "n"
        else
          newStr = newStr .. sstr
        end
      end
    
      grid[i] = newStr
    end
  
    WheatTimer = WheatTimer - Map.WheatTime
  end

  if ChickenTimer >= Map.ChickenTime then
  
    for i = 1, Map.Height do
      local str = grid[i]
      local newStr = ""
    
      for j = 1, Map.Width do
        local sstr = str:sub(j,j)
      
        if sstr == "w" then
          newStr = newStr .. "F"
        else
          newStr = newStr .. sstr
        end
      end
    
      grid[i] = newStr
    end
  
    ChickenTimer = ChickenTimer - Map.ChickenTime
  end

  if TurkeyTimer >= Map.TurkeyTime then
  
    for i = 1, Map.Height do
      local str = grid[i]
      local newStr = ""
    
      for j = 1, Map.Width do
        local sstr = str:sub(j,j)
      
        if sstr == "x" then
          newStr = newStr .. "G"
        else
          newStr = newStr .. sstr
        end
      end
    
      grid[i] = newStr
    end
  
    TurkeyTimer = TurkeyTimer - Map.TurkeyTime
  end

  if SheepTimer >= Map.SheepTime then
  
    for i = 1, Map.Height do
      local str = grid[i]
      local newStr = ""
    
      for j = 1, Map.Width do
        local sstr = str:sub(j,j)
      
        if sstr == "A" then
          newStr = newStr .. "H"
        else
          newStr = newStr .. sstr
        end
      end
    
      grid[i] = newStr
    end
  
    SheepTimer = SheepTimer - Map.SheepTime
  end

  if CowTimer >= Map.CowTime then
  
    for i = 1, Map.Height do
      local str = grid[i]
      local newStr = ""
    
      for j = 1, Map.Width do
        local sstr = str:sub(j,j)
      
        if sstr == "z" then
          newStr = newStr .. "I"
        else
          newStr = newStr .. sstr
        end
      end
    
      grid[i] = newStr
    end
  
    CowTimer = CowTimer - Map.CowTime
  end

  Map:CollectGold()
  
end

function Map:Reset()
  
  for i = 1, Map.Height do
    grid[i] = "ccccccccccccccccccccccccc"
  end

end

function Map:DrawMap()
  
  Xpos = 0
  Ypos = 0
  
  for i = 1, Map.Height do

    local str = grid[i]
    
    for j = 1, Map.Width do
      
      local sstr = str:sub(j, j)
      
      
      for c in sstr:gmatch("a") do
        love.graphics.draw(SmallTileSet, SmallQuads[1], (Xpos * SmallTileW), (Ypos * SmallTileH))
      end
      
      for c in sstr:gmatch("b") do
        love.graphics.draw(SmallTileSet, SmallQuads[2], (Xpos * SmallTileW), (Ypos * SmallTileH))
      end
      
      for c in sstr:gmatch("c") do
        love.graphics.draw(SmallTileSet, SmallQuads[3], (Xpos * SmallTileW), (Ypos * SmallTileH))
      end
      
      for c in sstr:gmatch("d") do
        love.graphics.draw(SmallTileSet, SmallQuads[4], (Xpos * SmallTileW), (Ypos * SmallTileH))
      end
      
      for c in sstr:gmatch("e") do
        love.graphics.draw(SmallTileSet, SmallQuads[5], (Xpos * SmallTileW), (Ypos * SmallTileH))
      end
      
      for c in sstr:gmatch("f") do
        love.graphics.draw(SmallTileSet, SmallQuads[6], (Xpos * SmallTileW), (Ypos * SmallTileH))
      end
      
      for c in sstr:gmatch("g") do
        love.graphics.draw(SmallTileSet, SmallQuads[7], (Xpos * SmallTileW), (Ypos * SmallTileH))
      end
      
      for c in sstr:gmatch("h") do
        love.graphics.draw(SmallTileSet, SmallQuads[8], (Xpos * SmallTileW), (Ypos * SmallTileH))
      end
      
      for c in sstr:gmatch("i") do
        love.graphics.draw(SmallTileSet, SmallQuads[9], (Xpos * SmallTileW), (Ypos * SmallTileH))
      end
      
      for c in sstr:gmatch("j") do
        love.graphics.draw(SmallTileSet, SmallQuads[10], (Xpos * SmallTileW), (Ypos * SmallTileH))
      end
      
      for c in sstr:gmatch("k") do
        love.graphics.draw(SmallTileSet, SmallQuads[11], (Xpos * SmallTileW), (Ypos * SmallTileH))
      end
      
      for c in sstr:gmatch("l") do
        love.graphics.draw(SmallTileSet, SmallQuads[12], (Xpos * SmallTileW), (Ypos * SmallTileH))
      end
      
      for c in sstr:gmatch("m") do
        love.graphics.draw(SmallTileSet, SmallQuads[13], (Xpos * SmallTileW), (Ypos * SmallTileH))
      end
      
      for c in sstr:gmatch("n") do
        love.graphics.draw(SmallTileSet, SmallQuads[14], (Xpos * SmallTileW), (Ypos * SmallTileH))
      end
      
      for c in sstr:gmatch("o") do
        love.graphics.draw(SmallTileSet, SmallQuads[15], (Xpos * SmallTileW), (Ypos * SmallTileH))
      end
      
      for c in sstr:gmatch("p") do
        love.graphics.draw(SmallTileSet, SmallQuads[16], (Xpos * SmallTileW), (Ypos * SmallTileH))
      end
      
      for c in sstr:gmatch("q") do
        love.graphics.draw(SmallTileSet, SmallQuads[17], (Xpos * SmallTileW), (Ypos * SmallTileH))
      end
      
      for c in sstr:gmatch("r") do
        love.graphics.draw(SmallTileSet, SmallQuads[18], (Xpos * SmallTileW), (Ypos * SmallTileH))
      end
      
      for c in sstr:gmatch("s") do
        love.graphics.draw(SmallTileSet, SmallQuads[19], (Xpos * SmallTileW), (Ypos * SmallTileH))
      end
      
      for c in sstr:gmatch("t") do
        love.graphics.draw(SmallTileSet, SmallQuads[20], (Xpos * SmallTileW), (Ypos * SmallTileH))
      end
      
      for c in sstr:gmatch("u") do
        love.graphics.draw(SmallTileSet, SmallQuads[21], (Xpos * SmallTileW), (Ypos * SmallTileH))
      end
      
      for c in sstr:gmatch("v") do
        love.graphics.draw(MediumTileSet, MediumQuads[1], (Xpos * SmallTileW), (Ypos * SmallTileH))
      end
      
      for c in sstr:gmatch("w") do
        love.graphics.draw(MediumTileSet, MediumQuads[2], (Xpos * SmallTileW), (Ypos * SmallTileH))
      end
      
      for c in sstr:gmatch("x") do
        love.graphics.draw(MediumTileSet, MediumQuads[3], (Xpos * SmallTileW), (Ypos * SmallTileH))
      end
      
      for c in sstr:gmatch("y") do
        love.graphics.draw(LargeTileSet, LargeQuads[1], (Xpos * SmallTileW), (Ypos * SmallTileH))
      end
      
      for c in sstr:gmatch("z") do
        love.graphics.draw(LargeTileSet, LargeQuads[2], (Xpos * SmallTileW), (Ypos * SmallTileH))
      end
      
      for c in sstr:gmatch("A") do
        love.graphics.draw(LargeTileSet, LargeQuads[3], (Xpos * SmallTileW), (Ypos * SmallTileH))
      end
      
      for c in sstr:gmatch("B") do
        love.graphics.draw(SmallTileSet, SmallQuads[22], (Xpos * SmallTileW), (Ypos * SmallTileH))
      end
      
      for c in sstr:gmatch("C") do
        love.graphics.draw(SmallTileSet, SmallQuads[23], (Xpos * SmallTileW), (Ypos * SmallTileH))
      end
      
      for c in sstr:gmatch("D") do
        love.graphics.draw(SmallTileSet, SmallQuads[24], (Xpos * SmallTileW), (Ypos * SmallTileH))
      end
      
      for c in sstr:gmatch("E") do
        love.graphics.draw(SmallTileSet, SmallQuads[25], (Xpos * SmallTileW), (Ypos * SmallTileH))
      end
      
      for c in sstr:gmatch("F") do
        love.graphics.draw(MediumTileSet, MediumQuads[4], (Xpos * SmallTileW), (Ypos * SmallTileH))
      end
      
      for c in sstr:gmatch("G") do
        love.graphics.draw(MediumTileSet, MediumQuads[5], (Xpos * SmallTileW), (Ypos * SmallTileH))
      end
      
      for c in sstr:gmatch("H") do
        love.graphics.draw(LargeTileSet, LargeQuads[4], (Xpos * SmallTileW), (Ypos * SmallTileH))
      end
      
      for c in sstr:gmatch("I") do
        love.graphics.draw(LargeTileSet, LargeQuads[5], (Xpos * SmallTileW), (Ypos * SmallTileH))
      end
      
      ::mapLoopSkip::
      
      if Xpos < Map.Width-1 then
        Xpos = Xpos +1
      else
        Xpos = 0
      end
      
    end
    
    Ypos = Ypos +1
    
  end
  
  if SelectedTile.X > 0 and SelectedTile.X < Map.Width and SelectedTile.Y > 0 and SelectedTile.Y < Map.Height then
    love.graphics.draw(deleteBtnSprite, (SelectedTile.X * SmallTileW) - 48, (SelectedTile.Y * SmallTileH), 0, 1, 1)
  end

  
end

function CheckCollision(x1,y1,w1,h1, x2,y2,w2,h2)
  return x1 < x2+w2 and
         x2 < x1+w1 and
         y1 < y2+h2 and
         y2 < y1+h1
end

function CheckMouseCollision(x1, y1, w, h, x2, y2)
  if x2 < x1 then
    return false
  end
  
  if x2 > x1 + w then
    return false
  end
  
  if y2 < y1 then
    return false
  end
  
  if y2 > y1 + h then
    return false
  end
  
  return true 
end
