require "Camera"

SoundSystem = {}


function SoundSystem:Initialise()
  SoundSystem.CowSound = love.audio.newSource("Sounds/Cow.wav")
  SoundSystem.ChickenSound = love.audio.newSource("Sounds/Chicken.wav")
  SoundSystem.SheepSound = love.audio.newSource("Sounds/Sheep.wav")
  SoundSystem.TurkeySound = love.audio.newSource("Sounds/Turkey.wav")
  SoundSystem.CoinSound = love.audio.newSource("Sounds/Coin.wav")
  SoundSystem.BackgroundMusic = love.audio.newSource("Sounds/BackgroundMusic.mp3")
  SoundSystem.BackgroundMusic:setVolume(0.1)
  SoundSystem.BackgroundMusic:setLooping(true)
  SoundSystem.BackgroundMusic:play()
end

function SoundSystem:PlaySound(sound)
  if sound == "cow" then
    SoundSystem.CowSound:stop()
    SoundSystem.CowSound:play()
  elseif sound == "chicken" then
    SoundSystem.ChickenSound:stop()
    SoundSystem.ChickenSound:play()
  elseif sound == "sheep" then
    SoundSystem.SheepSound:stop()
    SoundSystem.SheepSound:play()
  elseif sound == "turkey" then
    SoundSystem.TurkeySound:stop()
    SoundSystem.TurkeySound:play()
  elseif sound == "coin" then 
    SoundSystem.CoinSound:stop()
    SoundSystem.CoinSound:play()
  end
    
end

function SoundSystem:SetVolume(newVolume)
  love.audio:setVolume(newVolume)
end
