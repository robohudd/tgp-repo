Player = {}

function Player:Initialise()
  Player.gold = 100
  
  gold = love.graphics.newImage("Resources/Coin.png")
  display = love.graphics.newImage("Resources/CoinDisplay.png")
end

function Player:Update()
  
end

function Player:Draw()
  love.graphics.draw(display, 0, 0)
  love.graphics.draw(gold, 10, 15)
  love.graphics.print(Player.gold, 50, 25)
end

function Player:Reset()
  Player.gold = 100
  Map:Reset()
end

function Player:CurrentGold()
  return Player.gold
end

function Player:IncreaseGold(increaseAmount)
  Player.gold = Player.gold + increaseAmount
end

function Player:DecreaseGold(decreaseAmount)
  Player.gold = Player.gold - decreaseAmount
end