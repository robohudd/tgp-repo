Camera = {}
Camera.x = 0
Camera.y = 0
Camera.scaleX = 1
Camera.scaleY = 1
Camera.rotation = 0

prevX = 0
prevY = 0

function Camera:set()
  love.graphics.push()
  love.graphics.rotate(-self.rotation)
  love.graphics.scale(1 / self.scaleX, 1 / self.scaleY)
  love.graphics.translate(-self.x, -self.y)
  
  prevX, prevY = love.mouse.getPosition()
end

function Camera:unset()
  love.graphics.pop()
end

function Camera:move(dx, dy)
  self.x = self.x + (dx or 0)
  self.y = self.y + (dy or 0)
end

function Camera:Update()
   newCamX, newCamY = love.mouse.getPosition()
  
  if love.mouse.isDown(1) then
    Camera:move(prevX - newCamX, prevY - newCamY)
  end
  
  prevX = newCamX
  prevY = newCamY
end

function Camera:rotate(dr)
  self.rotation = self.rotation + dr
end

function Camera:scale(sx, sy)
  sx = sx or 1
  self.scaleX = self.scaleX * sx
  self.scaleY = self.scaleY * (sy or sx)
end

function Camera:setPosition(x, y)
  self.x = x or self.x
  self.y = y or self.y
end

function Camera:setScale(sx, sy)
  self.scaleX = sx or self.scaleX
  self.scaleY = sy or self.scaleY
  
  if self.scaleX < 1 or self.scaleY < 1 then
    self.scaleX = 1
    self.scaleY = 1
  end
  
  if self.scaleX > 5 or self.scaleY > 5 then
    self.scaleX = 5
    self.scaleY = 5
  end
end