require "MapSystem"

DragAndDrop = {}
selectedSprite = {}



function DragAndDrop:Initialise()
  
  selectedSprite.image = nil
  selectedSprite.quad = nil
  selectedSprite.value = "null"
end

function DragAndDrop:Update()

  if love.mouse.isDown(1) == false then
    
    if selectedSprite.value == "carrots" then
      Map:SetTileValue(Map:GetTileX(0), Map:GetTileY(0), "d", 1, CarrotCost)
    elseif selectedSprite.value == "potatos" then
      Map:SetTileValue(Map:GetTileX(0), Map:GetTileY(0), "j", 1, PotatoCost)
    elseif selectedSprite.value == "wheat" then
      Map:SetTileValue(Map:GetTileX(0), Map:GetTileY(0), "m", 1, WheatCost)
    elseif selectedSprite.value == "pineapples" then
      Map:SetTileValue(Map:GetTileX(0), Map:GetTileY(0), "g", 1, PineappleCost)
    elseif selectedSprite.value == "chickens" then
      Map:SetTileValue(Map:GetTileX(32), Map:GetTileY(32), "w", 2, ChickenCost)
    elseif selectedSprite.value == "turkeys" then
      Map:SetTileValue(Map:GetTileX(32), Map:GetTileY(32), "x", 2, TurkeyCost)
    elseif selectedSprite.value == "sheep" then
      Map:SetTileValue(Map:GetTileX(64), Map:GetTileY(64), "A", 3, SheepCost)
    elseif selectedSprite.value == "cows" then
      Map:SetTileValue(Map:GetTileX(64), Map:GetTileY(64), "z", 3, CowCost)
    elseif selectedSprite.value == "sprinkler" then
      Map:SetTileValue(Map:GetTileX(0), Map:GetTileY(0), "q", 1, SprinklerCost)
    elseif selectedSprite.value == "fertiliser" then
      Map:SetTileValue(Map:GetTileX(0), Map:GetTileY(0), "p", 1, FertiliserCost)
    elseif selectedSprite.value == "haybales" then
      Map:SetTileValue(Map:GetTileX(0), Map:GetTileY(0), "r", 1, HayBaleCost)
    elseif selectedSprite.value == "sheepdog" then
      Map:SetTileValue(Map:GetTileX(0), Map:GetTileY(0), "s", 1, SheepdogCost)
    elseif selectedSprite.value == "dirtPath" then
      Map:SetTileValue(Map:GetTileX(0), Map:GetTileY(0), "t", 1, DirtPathCost)
    elseif selectedSprite.value == "stonePath" then
      Map:SetTileValue(Map:GetTileX(0), Map:GetTileY(0), "u", 1, StonePathCost)
    elseif selectedSprite.value == "appleTree" then
      Map:SetTileValue(Map:GetTileX(0), Map:GetTileY(0), "a", 1, AppleTreeCost)
    elseif selectedSprite.value == "house" then
      Map:SetTileValue(Map:GetTileX(64), Map:GetTileY(64), "y", 3, HouseCost)
    end
    
    DragAndDrop:SelectSprite("null")
  end
  
end

function DragAndDrop:SelectSprite(selection)
  
  if selection == "carrots" then
    selectedSprite.image = SmallTileSet
    selectedSprite.quad = SmallQuads[4]
    selectedSprite.value = "carrots"
    selectedSprite.offset = 32
  elseif selection == "potatos" then
    selectedSprite.image = SmallTileSet
    selectedSprite.value = "potatos"
    selectedSprite.quad = SmallQuads[10]
    selectedSprite.offset = 32
  elseif selection == "wheat" then
    selectedSprite.image = SmallTileSet
    selectedSprite.value = "wheat"
    selectedSprite.quad = SmallQuads[13]
    selectedSprite.offset = 32
  elseif selection == "pineapples" then
    selectedSprite.image = SmallTileSet
    selectedSprite.value = "pineapples"
    selectedSprite.quad = SmallQuads[7]
    selectedSprite.offset = 32
  elseif selection == "chickens" then
    selectedSprite.image = MediumTileSet
    selectedSprite.value = "chickens"
    selectedSprite.quad = MediumQuads[2]
    selectedSprite.offset = 64
  elseif selection == "sheep" then
    selectedSprite.image = LargeTileSet
    selectedSprite.value = "sheep"
    selectedSprite.quad = LargeQuads[3]
    selectedSprite.offset = 96
  elseif selection == "cows" then
    selectedSprite.image = LargeTileSet
    selectedSprite.value = "cows"
    selectedSprite.quad = LargeQuads[2]
    selectedSprite.offset = 96
  elseif selection == "turkeys" then
    selectedSprite.image = MediumTileSet
    selectedSprite.value = "turkeys"
    selectedSprite.quad = MediumQuads[3]
    selectedSprite.offset = 64
  elseif selection == "sprinkler" then
    selectedSprite.image = SmallTileSet
    selectedSprite.value = "sprinkler"
    selectedSprite.quad = SmallQuads[17]
    selectedSprite.offset = 32
  elseif selection == "fertiliser" then
    selectedSprite.image = SmallTileSet
    selectedSprite.value = "fertiliser"
    selectedSprite.quad = SmallQuads[16]
    selectedSprite.offset = 32
  elseif selection == "haybales" then
    selectedSprite.image = SmallTileSet
    selectedSprite.value = "haybales"
    selectedSprite.quad = SmallQuads[18]
    selectedSprite.offset = 32
  elseif selection == "sheepdog" then
    selectedSprite.image = SmallTileSet
    selectedSprite.value = "sheepdog"
    selectedSprite.quad = SmallQuads[19]
    selectedSprite.offset = 32
  elseif selection == "dirtPath" then
    selectedSprite.image = SmallTileSet
    selectedSprite.value = "dirtPath"
    selectedSprite.quad = SmallQuads[20]
    selectedSprite.offset = 32
  elseif selection == "stonePath" then
    selectedSprite.image = SmallTileSet
    selectedSprite.value = "stonePath"
    selectedSprite.quad = SmallQuads[21]
    selectedSprite.offset = 32
  elseif selection == "appleTree" then
    selectedSprite.image = SmallTileSet
    selectedSprite.value = "appleTree"
    selectedSprite.quad = SmallQuads[1]
    selectedSprite.offset = 32
  elseif selection == "house" then
    selectedSprite.image = LargeTileSet
    selectedSprite.value = "house"
    selectedSprite.quad = LargeQuads[1]
    selectedSprite.offset = 96
  else
    selectedSprite.value = "null"
  end
  
end

function DragAndDrop:Draw()
  local mX, mY = love.mouse.getPosition()
  
  
  
  if selectedSprite.value == "null" then
    
  else
    love.graphics.draw(selectedSprite.image, selectedSprite.quad, mX - selectedSprite.offset, mY - selectedSprite.offset)
  end
  
  
end


